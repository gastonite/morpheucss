"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Morpheucss = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _focusable = require("./focusable.js");

var _pannable = require("./pannable.js");

var _zoomable = require("./zoomable.js");

var _noop = require("./utilities/noop.js");

var _transform = require("./utilities/transform.js");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var Morpheucss = function Morpheucss() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      container = _ref.container,
      transformElement = _ref.transformElement,
      zoomable = _ref.zoomable,
      pannable = _ref.pannable,
      focusable = _ref.focusable,
      contained = _ref.contained,
      minScale = _ref.minScale,
      maxScale = _ref.maxScale,
      whenFocus = _ref.whenFocus,
      whenFocused = _ref.whenFocused,
      whenUnfocus = _ref.whenUnfocus,
      whenUnfocused = _ref.whenUnfocused;

  if (typeof transformElement !== 'function') transformElement = (0, _transform.transform)(container.firstElementChild);
  return _objectSpread(_objectSpread(_objectSpread({
    zoom: _noop.noop,
    panBy: _noop.noop,
    focus: _noop.noop,
    unfocus: _noop.noop,
    reset: transformElement.bind(undefined, undefined)
  }, focusable && (0, _focusable.Focusable)({
    container: container,
    transformElement: transformElement,
    whenFocus: whenFocus,
    whenFocused: whenFocused,
    whenUnfocus: whenUnfocus,
    whenUnfocused: whenUnfocused
  })), zoomable && (0, _zoomable.Zoomable)({
    container: container,
    transformElement: transformElement,
    contained: contained,
    minScale: minScale,
    maxScale: maxScale
  })), pannable && (0, _pannable.Pannable)({
    container: container,
    transformElement: transformElement,
    contained: contained
  }));
};

exports.Morpheucss = Morpheucss;