"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Zoomable = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _delta = require("./vector/delta.js");

var _vector = require("./vector/vector.js");

var _changeOrigin = require("./utilities/changeOrigin.js");

var _limitAbove = require("./utilities/limitAbove.js");

var _limitBelow = require("./utilities/limitBelow.js");

var _project = require("./utilities/project.js");

var _transform = require("./utilities/transform.js");

var _concatVectors2 = require("./utilities/concatVectors.js");

var _unproject = require("./utilities/unproject.js");

var _Transformation = require("./utilities/Transformation.js");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var Zoomable = function Zoomable(_ref) {
  var container = _ref.container,
      _ref$minScale = _ref.minScale,
      minScale = _ref$minScale === void 0 ? 0 : _ref$minScale,
      _ref$maxScale = _ref.maxScale,
      maxScale = _ref$maxScale === void 0 ? Infinity : _ref$maxScale,
      transformElement = _ref.transformElement,
      _ref$contained = _ref.contained,
      contained = _ref$contained === void 0 ? false : _ref$contained;
  var area = container.firstElementChild;
  if (typeof transformElement !== 'function') transformElement = (0, _transform.transform)(area);
  if (contained) minScale = 1;

  var zoom = /*#__PURE__*/function () {
    var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref2) {
      var x, y, ratio, transformation, scale, _container$getBoundin, width, height, changeTransformOrigin, _transformation, translateX, translateY, nextTransformation, offsetWidth, offsetHeight, end, _concatVectors;

      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              x = _ref2.x, y = _ref2.y, ratio = _ref2.ratio, transformation = _ref2.transformation, scale = _ref2.scale;
              _container$getBoundin = container.getBoundingClientRect(), width = _container$getBoundin.width, height = _container$getBoundin.height;
              if (typeof x !== 'number') x = width / 2;
              if (typeof y !== 'number') y = height / 2; // Parse options

              transformation = (0, _Transformation.Transformation)(transformation);
              changeTransformOrigin = (0, _changeOrigin.changeOrigin)(transformation);

              if (typeof scale !== 'number') {
                if (typeof ratio !== 'number') ratio = 1;
                scale = transformation.scale * ratio;
              } // Constrains scale


              scale = Math.max(minScale, Math.min(scale, maxScale)); // Redefine origin

              transformation = changeTransformOrigin((0, _vector.Vector)({
                x: x,
                y: y
              }).map((0, _unproject.unproject)(transformation))); // Adjust translation

              _transformation = transformation, translateX = _transformation.translateX, translateY = _transformation.translateY;
              nextTransformation = _objectSpread(_objectSpread({}, transformation), {}, {
                scale: scale
              });

              if (contained) {
                offsetWidth = container.offsetWidth, offsetHeight = container.offsetHeight;
                end = (0, _vector.Vector)({
                  x: offsetWidth,
                  y: offsetHeight
                });
                _concatVectors = (0, _concatVectors2.concatVectors)({
                  x: translateX,
                  y: translateY
                }, _vector.origin.map((0, _project.project)(nextTransformation)).map((0, _limitAbove.limitAbove)(_vector.origin)).map((0, _delta.toDelta)(_vector.origin)), end.map((0, _project.project)(nextTransformation)).map((0, _limitBelow.limitBelow)(end)).map((0, _delta.toDelta)(end)));
                translateX = _concatVectors.x;
                translateY = _concatVectors.y;
              } // Apply transformation


              _context.next = 14;
              return transformElement(_objectSpread(_objectSpread({}, nextTransformation), {}, {
                translateX: translateX,
                translateY: translateY
              }));

            case 14:
              return _context.abrupt("return", _context.sent);

            case 15:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function zoom(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  return {
    zoom: zoom
  };
};

exports.Zoomable = Zoomable;