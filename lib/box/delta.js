"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toDeltaBox = void 0;

var _delta = require("../vector/delta.js");

var toDeltaBox = function toDeltaBox(_ref) {
  var from0 = _ref.from,
      to0 = _ref.to;
  return function (_ref2) {
    var _ref2$from = _ref2.from,
        from1 = _ref2$from === void 0 ? 0 : _ref2$from,
        _ref2$to = _ref2.to,
        to1 = _ref2$to === void 0 ? 0 : _ref2$to;
    return {
      from: (0, _delta.toDelta)(from0)(from1),
      to: (0, _delta.toDelta)(to0)(to1)
    };
  };
};

exports.toDeltaBox = toDeltaBox;