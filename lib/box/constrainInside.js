"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.constrainInside = void 0;

var constrainInside = function constrainInside(_ref) {
  var _ref$from = _ref.from,
      xMin = _ref$from.x,
      yMin = _ref$from.y,
      _ref$to = _ref.to,
      xMax = _ref$to.x,
      yMax = _ref$to.y;
  return function (_ref2) {
    var _ref2$from = _ref2.from,
        fromX = _ref2$from.x,
        fromY = _ref2$from.y,
        _ref2$to = _ref2.to,
        toX = _ref2$to.x,
        toY = _ref2$to.y;
    return {
      from: {
        x: fromX < xMin ? xMin : fromX,
        y: fromY < yMin ? yMin : fromY
      },
      to: {
        x: toX > xMax ? xMax : toX,
        y: toY > yMax ? yMax : toY
      }
    };
  };
};

exports.constrainInside = constrainInside;