"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Box = void 0;

var _identity = require("../utilities/identity.js");

var _vector = require("../vector/vector.js");

var Box = function Box() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$from = _ref.from,
      from = _ref$from === void 0 ? _vector.origin : _ref$from,
      _ref$to = _ref.to,
      to = _ref$to === void 0 ? _vector.origin : _ref$to;

  var box = {
    from: (0, _vector.Vector)(from),
    to: (0, _vector.Vector)(to),
    map: function map() {
      var f = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _identity.identity;
      return Box(f(box));
    }
  };
  return box;
};

exports.Box = Box;