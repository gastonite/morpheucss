"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.projectBox = void 0;

var _project = require("../utilities/project.js");

var _box = require("./box.js");

var projectBox = function projectBox(transformation) {
  return function (box) {
    var _Box = (0, _box.Box)(box),
        from = _Box.from,
        to = _Box.to;

    return {
      from: from.map((0, _project.project)(transformation)),
      to: to.map((0, _project.project)(transformation))
    };
  };
};

exports.projectBox = projectBox;