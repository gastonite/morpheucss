"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.constrainOutside = void 0;

var constrainOutside = function constrainOutside(_ref) {
  var _ref$from = _ref.from,
      xMax = _ref$from.x,
      yMax = _ref$from.y,
      _ref$to = _ref.to,
      xMin = _ref$to.x,
      yMin = _ref$to.y;
  return function (_ref2) {
    var _ref2$from = _ref2.from,
        fromX = _ref2$from.x,
        fromY = _ref2$from.y,
        _ref2$to = _ref2.to,
        toX = _ref2$to.x,
        toY = _ref2$to.y;
    return {
      from: {
        x: fromX > xMax ? xMax : fromX,
        y: fromY > yMax ? yMax : fromY
      },
      to: {
        x: toX < xMin ? xMin : toX,
        y: toY < yMin ? yMin : toY
      }
    };
  };
};

exports.constrainOutside = constrainOutside;