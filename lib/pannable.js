"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Pannable = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _box = require("./box/box.js");

var _constrainInside = require("./box/constrainInside.js");

var _delta = require("./box/delta.js");

var _vector = require("./vector/vector.js");

var _transform = require("./utilities/transform.js");

var _project = require("./box/project.js");

var _Transformation = require("./utilities/Transformation.js");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var Pannable = function Pannable(_ref) {
  var container = _ref.container,
      transformElement = _ref.transformElement,
      _ref$contained = _ref.contained,
      contained = _ref$contained === void 0 ? false : _ref$contained;
  var area = container.firstElementChild;
  if (typeof transformElement !== 'function') transformElement = (0, _transform.transform)(area);

  var panBy = /*#__PURE__*/function () {
    var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref2) {
      var x, y, transformation, _container$getBoundin, width, height, bottomRight, containerBox, _transformation, translateX, translateY, translation, deltaBox;

      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              x = _ref2.deltaX, y = _ref2.deltaY, transformation = _ref2.transformation;
              transformation = (0, _Transformation.Transformation)(transformation);
              _container$getBoundin = container.getBoundingClientRect(), width = _container$getBoundin.width, height = _container$getBoundin.height;
              bottomRight = (0, _vector.Vector)({
                x: width,
                y: height
              });
              containerBox = (0, _box.Box)({
                from: _vector.origin,
                to: bottomRight
              });
              _transformation = transformation, translateX = _transformation.translateX, translateY = _transformation.translateY;
              translation = _objectSpread(_objectSpread({}, transformation), {}, {
                translateX: translateX + x,
                translateY: translateY + y
              });

              if (contained) {
                deltaBox = containerBox.map((0, _project.projectBox)(translation)).map((0, _constrainInside.constrainInside)(containerBox)).map((0, _delta.toDeltaBox)(containerBox));
                x += deltaBox.from.x + deltaBox.to.x;
                y += deltaBox.from.y + deltaBox.to.y;
              }

              _context.next = 10;
              return transformElement(_objectSpread(_objectSpread({}, transformation), {}, {
                translateX: translateX + x,
                translateY: translateY + y,
                animated: false
              }));

            case 10:
              return _context.abrupt("return", _context.sent);

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function panBy(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  return {
    panBy: panBy
  };
};

exports.Pannable = Pannable;