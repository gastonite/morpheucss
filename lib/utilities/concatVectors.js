"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.concatVectors = void 0;

var _vector = require("../vector/vector.js");

var concatVectors = function concatVectors() {
  for (var _len = arguments.length, points = new Array(_len), _key = 0; _key < _len; _key++) {
    points[_key] = arguments[_key];
  }

  return points.reduce(function (origin, point) {
    var x0 = origin.x,
        y0 = origin.y;
    var x = point.x,
        y = point.y;
    return {
      x: x0 + x,
      y: y0 + y
    };
  }, _vector.origin);
};

exports.concatVectors = concatVectors;