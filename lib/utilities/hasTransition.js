"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hasTransition = void 0;

var hasTransition = function hasTransition(element) {
  var style = window.getComputedStyle(element);
  return style.getPropertyValue('transition-duration') !== '0s' && style.getPropertyValue('transition-property') !== 'none';
};

exports.hasTransition = hasTransition;