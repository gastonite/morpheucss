"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.always = void 0;

var always = function always(x) {
  return function () {
    return x;
  };
};

exports.always = always;