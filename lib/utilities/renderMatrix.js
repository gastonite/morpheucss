"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderMatrix = void 0;

var _Transformation2 = require("./Transformation");

var renderMatrix = function renderMatrix(transformation) {
  var _Transformation = (0, _Transformation2.Transformation)(transformation),
      translateX = _Transformation.translateX,
      translateY = _Transformation.translateY,
      scale = _Transformation.scale;

  return "matrix(".concat(scale, ", 0, 0, ").concat(scale, ", ").concat(translateX, ", ").concat(translateY, ")");
};

exports.renderMatrix = renderMatrix;