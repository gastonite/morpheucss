"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isNumber = void 0;

var isNumber = function isNumber(number) {
  return typeof number === 'number';
};

exports.isNumber = isNumber;