"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.derive = void 0;

var _Transformation2 = require("./Transformation");

var derive = function derive(transformation) {
  var _Transformation = (0, _Transformation2.Transformation)(transformation),
      translateX = _Transformation.translateX,
      translateY = _Transformation.translateY,
      scale = _Transformation.scale;

  var derive = function derive() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$x = _ref.x,
        x = _ref$x === void 0 ? 0 : _ref$x,
        _ref$y = _ref.y,
        y = _ref$y === void 0 ? 0 : _ref$y;

    return {
      x: scale * x + translateX,
      y: scale * y + translateY
    };
  };

  return derive;
};

exports.derive = derive;