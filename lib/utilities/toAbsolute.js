"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toAbsolute = void 0;

var toAbsolute = function toAbsolute(_ref) {
  var _ref$originX = _ref.originX,
      originX = _ref$originX === void 0 ? 0 : _ref$originX,
      _ref$originY = _ref.originY,
      originY = _ref$originY === void 0 ? 0 : _ref$originY;
  return function () {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref2$x = _ref2.x,
        x = _ref2$x === void 0 ? 0 : _ref2$x,
        _ref2$y = _ref2.y,
        y = _ref2$y === void 0 ? 0 : _ref2$y;

    return {
      x: originX + x,
      y: originY + y
    };
  };
};

exports.toAbsolute = toAbsolute;