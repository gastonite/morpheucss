"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.waitNextTick = void 0;

var waitNextTick = function waitNextTick() {
  return new Promise(setTimeout);
};

exports.waitNextTick = waitNextTick;