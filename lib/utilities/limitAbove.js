"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.limitAbove = void 0;

var limitAbove = function limitAbove() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$x = _ref.x,
      xMin = _ref$x === void 0 ? 0 : _ref$x,
      _ref$y = _ref.y,
      yMin = _ref$y === void 0 ? 0 : _ref$y;

  return function () {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref2$x = _ref2.x,
        x = _ref2$x === void 0 ? 0 : _ref2$x,
        _ref2$y = _ref2.y,
        y = _ref2$y === void 0 ? 0 : _ref2$y;

    return {
      x: x < xMin ? xMin : x,
      y: y < yMin ? yMin : y
    };
  };
};

exports.limitAbove = limitAbove;