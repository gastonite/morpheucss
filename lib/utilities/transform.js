"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transform = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _waitNextTick = require("./waitNextTick.js");

var _renderMatrix = require("./renderMatrix.js");

var _Transformation = require("./Transformation.js");

var transform = function transform(element) {
  var style = element.style;

  var transformElement = /*#__PURE__*/function () {
    var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(transformation) {
      var _transformation, originX, originY;

      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _transformation = transformation = (0, _Transformation.Transformation)(transformation), originX = _transformation.originX, originY = _transformation.originY;
              style.transformOrigin = "".concat(originX, "px ").concat(originY, "px");
              style.transform = (0, _renderMatrix.renderMatrix)(transformation);
              _context.next = 5;
              return (0, _waitNextTick.waitNextTick)();

            case 5:
              return _context.abrupt("return", transformation);

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function transformElement(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  return transformElement;
};

exports.transform = transform;