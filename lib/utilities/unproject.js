"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.unproject = void 0;

var _vector = require("../vector/vector.js");

var _toAbsolute = require("./toAbsolute.js");

var _toRelative = require("./toRelative.js");

var _Transformation2 = require("./Transformation.js");

var _underive = require("./underive.js");

var unproject = function unproject(transformation) {
  var _Transformation = (0, _Transformation2.Transformation)(transformation),
      originX = _Transformation.originX,
      originY = _Transformation.originY,
      translateX = _Transformation.translateX,
      translateY = _Transformation.translateY,
      scale = _Transformation.scale;

  var unproject = function unproject(point) {
    return (0, _vector.Vector)(point).map((0, _toRelative.toRelative)({
      originX: originX,
      originY: originY
    })).map((0, _underive.underive)({
      scale: scale,
      translateX: translateX,
      translateY: translateY
    })).map((0, _toAbsolute.toAbsolute)({
      originX: originX,
      originY: originY
    }));
  };

  return unproject;
};

exports.unproject = unproject;