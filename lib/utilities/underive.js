"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.underive = void 0;

var _Transformation2 = require("./Transformation");

var underive = function underive(transformation) {
  var _Transformation = (0, _Transformation2.Transformation)(transformation),
      translateX = _Transformation.translateX,
      translateY = _Transformation.translateY,
      scale = _Transformation.scale;

  var underive = function underive() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$x = _ref.x,
        x = _ref$x === void 0 ? 0 : _ref$x,
        _ref$y = _ref.y,
        y = _ref$y === void 0 ? 0 : _ref$y;

    return {
      x: (x - translateX) / scale,
      y: (y - translateY) / scale
    };
  };

  return underive;
};

exports.underive = underive;