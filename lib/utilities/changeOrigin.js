"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.changeOrigin = void 0;

var _vector = require("../vector/vector.js");

var _Transformation2 = require("./Transformation.js");

var changeOrigin = function changeOrigin(transformation) {
  var _Transformation = (0, _Transformation2.Transformation)(transformation),
      originX = _Transformation.originX,
      originY = _Transformation.originY,
      scale = _Transformation.scale,
      translateX = _Transformation.translateX,
      translateY = _Transformation.translateY;

  var changeOrigin = function changeOrigin(point) {
    var _Vector = (0, _vector.Vector)(point),
        x = _Vector.x,
        y = _Vector.y;

    var deltaX = x - originX;
    var deltaY = y - originY;
    return {
      originX: x,
      originY: y,
      translateX: deltaX * (scale - 1) + translateX,
      translateY: deltaY * (scale - 1) + translateY,
      scale: scale
    };
  };

  return changeOrigin;
};

exports.changeOrigin = changeOrigin;