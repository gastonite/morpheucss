"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Transformation = void 0;

var Transformation = function Transformation() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$originX = _ref.originX,
      originX = _ref$originX === void 0 ? 0 : _ref$originX,
      _ref$originY = _ref.originY,
      originY = _ref$originY === void 0 ? 0 : _ref$originY,
      _ref$translateX = _ref.translateX,
      translateX = _ref$translateX === void 0 ? 0 : _ref$translateX,
      _ref$translateY = _ref.translateY,
      translateY = _ref$translateY === void 0 ? 0 : _ref$translateY,
      _ref$scale = _ref.scale,
      scale = _ref$scale === void 0 ? 1 : _ref$scale;

  return {
    originX: originX,
    originY: originY,
    translateX: translateX,
    translateY: translateY,
    scale: scale
  };
};

exports.Transformation = Transformation;