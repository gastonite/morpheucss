"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.waitTransitionEnd = void 0;

var waitTransitionEnd = function waitTransitionEnd(element) {
  return new Promise(function (resolve) {
    var onTransitionEnd = function onTransitionEnd(event) {
      if (event.target !== element) return;
      element.removeEventListener('transitionend', onTransitionEnd);
      resolve(event);
    };

    element.addEventListener('transitionend', onTransitionEnd);
  });
};

exports.waitTransitionEnd = waitTransitionEnd;