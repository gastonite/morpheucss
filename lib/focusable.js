"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Focusable = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _vector = require("./vector/vector.js");

var _delta = require("./vector/delta.js");

var _waitNextTick = require("./utilities/waitNextTick.js");

var _transform = require("./utilities/transform.js");

var _changeOrigin = require("./utilities/changeOrigin.js");

var _project = require("./utilities/project.js");

var _noop = require("./utilities/noop.js");

var _Transformation = require("./utilities/Transformation.js");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var Focusable = function Focusable(_ref) {
  var container = _ref.container,
      _ref$transformation = _ref.transformation,
      transformation = _ref$transformation === void 0 ? {} : _ref$transformation,
      transformElement = _ref.transformElement,
      _ref$fit = _ref.fit,
      defaultFit = _ref$fit === void 0 ? 'auto' : _ref$fit,
      _ref$whenFocus = _ref.whenFocus,
      whenFocus = _ref$whenFocus === void 0 ? _waitNextTick.waitNextTick : _ref$whenFocus,
      _ref$whenUnfocus = _ref.whenUnfocus,
      whenUnfocus = _ref$whenUnfocus === void 0 ? _waitNextTick.waitNextTick : _ref$whenUnfocus,
      _ref$whenFocused = _ref.whenFocused,
      whenFocused = _ref$whenFocused === void 0 ? _noop.noop : _ref$whenFocused,
      _ref$whenUnfocused = _ref.whenUnfocused,
      whenUnfocused = _ref$whenUnfocused === void 0 ? _waitNextTick.waitNextTick : _ref$whenUnfocused;
  var area = container.firstElementChild;
  if (typeof transformElement !== 'function') transformElement = (0, _transform.transform)(area);

  var _focused;

  var _focusing;

  var _unfocus = /*#__PURE__*/function () {
    var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!_focusing) {
                _context.next = 2;
                break;
              }

              return _context.abrupt("return");

            case 2:
              if (_focused) {
                _context.next = 4;
                break;
              }

              return _context.abrupt("return");

            case 4:
              _focusing = true;
              _context.next = 7;
              return whenUnfocus();

            case 7:
              _context.t0 = Object;
              _context.t1 = transformation;
              _context.next = 11;
              return transformElement();

            case 11:
              _context.t2 = _context.sent;

              _context.t0.assign.call(_context.t0, _context.t1, _context.t2);

              _context.next = 15;
              return whenUnfocused();

            case 15:
              _focused = false;
              _focusing = false;

            case 17:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function _unfocus() {
      return _ref2.apply(this, arguments);
    };
  }();

  var focus = /*#__PURE__*/function () {
    var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
      var _ref4,
          target,
          _ref4$fit,
          fit,
          transformation,
          changeTransformOrigin,
          _container$getBoundin,
          width,
          height,
          _target$getBoundingCl,
          targetWidth,
          targetHeight,
          xRatio,
          yRatio,
          ratio,
          scale,
          offsetTop,
          offsetLeft,
          targetOrigin,
          projectScale,
          originImage,
          delta,
          _args2 = arguments;

      return _regenerator["default"].wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _ref4 = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {}, target = _ref4.target, _ref4$fit = _ref4.fit, fit = _ref4$fit === void 0 ? defaultFit : _ref4$fit, transformation = _ref4.transformation;

              if (!_focusing) {
                _context2.next = 3;
                break;
              }

              return _context2.abrupt("return");

            case 3:
              if (!_focused) {
                _context2.next = 5;
                break;
              }

              return _context2.abrupt("return", _unfocus());

            case 5:
              _focusing = true;
              transformation = (0, _Transformation.Transformation)(transformation);
              changeTransformOrigin = (0, _changeOrigin.changeOrigin)(transformation); // Determine scale

              _container$getBoundin = container.getBoundingClientRect(), width = _container$getBoundin.width, height = _container$getBoundin.height;
              _target$getBoundingCl = target.getBoundingClientRect(), targetWidth = _target$getBoundingCl.width, targetHeight = _target$getBoundingCl.height;
              xRatio = width / targetWidth;
              yRatio = height / targetHeight;
              ratio = fit === 'height' ? yRatio : fit === 'width' ? xRatio : Math.max(xRatio, yRatio);
              scale = transformation.scale * ratio; // Redefine origin

              _context2.next = 16;
              return transformElement(changeTransformOrigin(transformation));

            case 16:
              transformation = _context2.sent;
              // Calculate delta vector
              offsetTop = target.offsetTop, offsetLeft = target.offsetLeft;
              targetOrigin = (0, _vector.Vector)({
                x: offsetLeft,
                y: offsetTop
              });
              projectScale = (0, _project.project)(_objectSpread(_objectSpread({}, transformation), {}, {
                scale: scale
              }));
              originImage = _vector.origin.map(projectScale);
              delta = targetOrigin.map(projectScale).map((0, _delta.toDelta)(originImage)); // Apply transformation

              _context2.next = 24;
              return whenFocus();

            case 24:
              _context2.next = 26;
              return transformElement(_objectSpread(_objectSpread({}, transformation), {}, {
                scale: scale,
                translateX: delta.x,
                translateY: delta.y
              }));

            case 26:
              transformation = _context2.sent;
              _context2.next = 29;
              return whenFocused();

            case 29:
              _focusing = false;
              _focused = true;
              return _context2.abrupt("return", transformation);

            case 32:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function focus() {
      return _ref3.apply(this, arguments);
    };
  }();

  return {
    focus: focus
  };
};

exports.Focusable = Focusable;