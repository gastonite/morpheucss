"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toDelta = void 0;

var toDelta = function toDelta(_ref) {
  var _ref$x = _ref.x,
      x0 = _ref$x === void 0 ? 0 : _ref$x,
      _ref$y = _ref.y,
      y0 = _ref$y === void 0 ? 0 : _ref$y;
  return function (_ref2) {
    var _ref2$x = _ref2.x,
        x1 = _ref2$x === void 0 ? 0 : _ref2$x,
        _ref2$y = _ref2.y,
        y1 = _ref2$y === void 0 ? 0 : _ref2$y;
    return {
      x: x0 - x1,
      y: y0 - y1
    };
  };
};

exports.toDelta = toDelta;