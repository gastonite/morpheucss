"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.origin = exports.Vector = void 0;

var _identity = require("../utilities/identity.js");

var Vector = function Vector() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$x = _ref.x,
      x = _ref$x === void 0 ? 0 : _ref$x,
      _ref$y = _ref.y,
      y = _ref$y === void 0 ? 0 : _ref$y;

  var point = {
    x: x,
    y: y,
    map: function map() {
      var f = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _identity.identity;
      return Vector(f(point));
    },
    toArray: function toArray() {
      return [x, y];
    }
  };
  return point;
};

exports.Vector = Vector;
var origin = Vector();
exports.origin = origin;