




export const toDelta = ({
  x: x0 = 0,
  y: y0 = 0
}) => ({
  x: x1 = 0,
  y: y1 = 0
}) => ({
  x: x0 - x1,
  y: y0 - y1,
})