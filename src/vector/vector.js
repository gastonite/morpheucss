import { identity } from '../utilities/identity.js'





export const Vector = ({
  x = 0,
  y = 0,
} = {}) => {

  const point = {
    x,
    y,
    map: (f = identity) => Vector(f(point)),
    toArray: () => [x, y],
  }

  return point
}

export const origin = Vector()
