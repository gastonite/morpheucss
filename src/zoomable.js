import { toDelta } from './vector/delta.js'
import { origin, Vector } from './vector/vector.js'
import { changeOrigin } from './utilities/changeOrigin.js'
import { limitAbove } from './utilities/limitAbove.js'
import { limitBelow } from './utilities/limitBelow.js'
import { project } from './utilities/project.js'
import { transform } from './utilities/transform.js'
import { concatVectors } from './utilities/concatVectors.js'
import { unproject } from './utilities/unproject.js'
import { Transformation } from './utilities/Transformation.js'








export const Zoomable = ({
  container,
  minScale = 0,
  maxScale = Infinity,
  transformElement,
  contained = false,
}) => {

  const { firstElementChild: area } = container

  if (typeof transformElement !== 'function')
    transformElement = transform(area)

  if (contained)
    minScale = 1

  const zoom = async ({ x, y, ratio, transformation, scale }) => {

    const { width, height } = container.getBoundingClientRect()

    if (typeof x !== 'number')
      x = width / 2
    if (typeof y !== 'number')
      y = height / 2

    // Parse options
    transformation = Transformation(transformation)
    const changeTransformOrigin = changeOrigin(transformation)

    if (typeof scale !== 'number') {
      if (typeof ratio !== 'number')
        ratio = 1

      scale = transformation.scale * ratio
    }

    // Constrains scale
    scale = Math.max(minScale, Math.min(scale, maxScale))

    // Redefine origin
    transformation = changeTransformOrigin(
      Vector({ x, y })
        .map(unproject(transformation))
    )

    // Adjust translation
    let { translateX, translateY } = transformation
    const nextTransformation = { ...transformation, scale }

    if (contained) {

      const { offsetWidth, offsetHeight } = container
      const end = Vector({ x: offsetWidth, y: offsetHeight });

      ({ x: translateX, y: translateY } = concatVectors(
        { x: translateX, y: translateY },
        origin
          .map(project(nextTransformation))
          .map(limitAbove(origin))
          .map(toDelta(origin)),
        end
          .map(project(nextTransformation))
          .map(limitBelow(end))
          .map(toDelta(end)),
      ))
    }

    // Apply transformation
    return await transformElement({
      ...nextTransformation,
      translateX,
      translateY,
    })
  }

  return {
    zoom
  }
}
