import { Box } from './box/box.js'
import { constrainInside } from './box/constrainInside.js'
import { toDeltaBox } from './box/delta.js'
import { origin, Vector } from './vector/vector.js'
import { transform } from './utilities/transform.js'
import { projectBox } from './box/project.js'
import { Transformation } from './utilities/Transformation.js'





export const Pannable = ({
  container,
  transformElement,
  contained = false,
}) => {

  const { firstElementChild: area } = container

  if (typeof transformElement !== 'function')
    transformElement = transform(area)

  const panBy = async ({ deltaX: x, deltaY: y, transformation, }) => {

    transformation = Transformation(transformation)
    const { width, height } = container.getBoundingClientRect()
    const bottomRight = Vector({ x: width, y: height })
    const containerBox = Box({ from: origin, to: bottomRight })
    let { translateX, translateY } = transformation

    let translation = {
      ...transformation,
      translateX: translateX + x,
      translateY: translateY + y,
    }

    if (contained) {

      const deltaBox = containerBox.map(projectBox(translation))
        .map(constrainInside(containerBox))
        .map(toDeltaBox(containerBox))

      x += deltaBox.from.x + deltaBox.to.x
      y += deltaBox.from.y + deltaBox.to.y
    }

    return await transformElement({
      ...transformation,
      translateX: translateX + x,
      translateY: translateY + y,
      animated: false
    })
  }

  return {
    panBy
  }
}
