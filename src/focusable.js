import { Vector, origin } from './vector/vector.js'
import { toDelta } from './vector/delta.js'
import { waitNextTick } from './utilities/waitNextTick.js'
import { transform } from './utilities/transform.js'
import { changeOrigin } from './utilities/changeOrigin.js'
import { project } from './utilities/project.js'
import { noop } from './utilities/noop.js'
import { Transformation } from './utilities/Transformation.js'





export const Focusable = ({
  container,
  transformation = {},
  transformElement,
  fit: defaultFit = 'auto',
  whenFocus = waitNextTick,
  whenUnfocus = waitNextTick,
  whenFocused = noop,
  whenUnfocused = waitNextTick,
}) => {

  const { firstElementChild: area } = container

  if (typeof transformElement !== 'function')
    transformElement = transform(area)

  let _focused
  let _focusing

  const _unfocus = async () => {

    if (_focusing)
      return

    if (!_focused)
      return

    _focusing = true

    await whenUnfocus()

    Object.assign(transformation, await transformElement())

    await whenUnfocused()

    _focused = false
    _focusing = false
  }

  const focus = async ({
    target,
    fit = defaultFit,
    transformation
  } = {}) => {

    if (_focusing)
      return

    if (_focused)
      return _unfocus()

    _focusing = true

    transformation = Transformation(transformation)
    const changeTransformOrigin = changeOrigin(transformation)

    // Determine scale
    const { width, height } = container.getBoundingClientRect()
    const { width: targetWidth, height: targetHeight } = target.getBoundingClientRect()

    const xRatio = width / targetWidth
    const yRatio = height / targetHeight

    const ratio = fit === 'height'
      ? yRatio
      : fit === 'width'
        ? xRatio
        : Math.max(xRatio, yRatio)

    const scale = transformation.scale * ratio

    // Redefine origin
    transformation = await transformElement(changeTransformOrigin(transformation))

    // Calculate delta vector
    const { offsetTop, offsetLeft } = target
    const targetOrigin = Vector({ x: offsetLeft, y: offsetTop })
    const projectScale = project({ ...transformation, scale })

    const originImage = origin
      .map(projectScale)

    const delta = targetOrigin
      .map(projectScale)
      .map(toDelta(originImage))

    // Apply transformation
    await whenFocus()

    transformation = await transformElement({
      ...transformation,
      scale,
      translateX: delta.x,
      translateY: delta.y,
    })

    await whenFocused()

    _focusing = false
    _focused = true

    return transformation
  }

  return {
    focus,
  }
}
