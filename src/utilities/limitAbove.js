




export const limitAbove = ({
  x: xMin = 0,
  y: yMin = 0
} = {}) => ({
  x = 0,
  y = 0
} = {}) => ({
  x: x < xMin
    ? xMin
    : x,
  y: y < yMin
    ? yMin
    : y,
})
