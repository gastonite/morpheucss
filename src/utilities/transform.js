import { waitNextTick } from './waitNextTick.js'
import { renderMatrix } from './renderMatrix.js'
import { Transformation } from './Transformation.js'





export const transform = element => {

  const { style } = element

  const transformElement = async transformation => {

    const { originX, originY } = transformation = Transformation(transformation)

    style.transformOrigin = `${originX}px ${originY}px`
    style.transform = renderMatrix(transformation)

    await waitNextTick()

    return transformation
  }

  return transformElement
}

