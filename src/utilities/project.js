import { Vector } from '../vector/vector.js'
import { derive } from './derive.js'
import { toAbsolute } from './toAbsolute.js'
import { toRelative } from './toRelative.js'
import { Transformation } from './Transformation.js'





export const project = transformation => {
  const {
    originX,
    originY,
    translateX,
    translateY,
    scale,
  } = Transformation(transformation)

  const project = point => {

    return Vector(point)
      .map(toRelative({ originX, originY }))
      .map(derive({
        scale,
        translateX,
        translateY,
      }))
      .map(toAbsolute({ originX, originY }))
  }

  return project
}