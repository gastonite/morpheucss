import { Vector } from '../vector/vector.js'
import { Transformation } from './Transformation.js'





export const changeOrigin = transformation => {

  const {
    originX,
    originY,
    scale,
    translateX,
    translateY
  } = Transformation(transformation)

  const changeOrigin = point => {

    const { x, y } = Vector(point)
    const deltaX = x - originX
    const deltaY = y - originY

    return {
      originX: x,
      originY: y,
      translateX: deltaX * (scale - 1) + translateX,
      translateY: deltaY * (scale - 1) + translateY,
      scale
    }
  }

  return changeOrigin
}
