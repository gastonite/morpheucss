import { origin } from '../vector/vector.js'





export const concatVectors = (...points) => points.reduce((origin, point) => {

  const { x: x0, y: y0 } = origin
  const { x, y } = point

  return {
    x: x0 + x,
    y: y0 + y,
  }
}, origin)