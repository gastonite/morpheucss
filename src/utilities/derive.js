import { Transformation } from './Transformation'





export const derive = transformation => {

  const {
    translateX,
    translateY,
    scale,
  } = Transformation(transformation)

  const derive = ({ x = 0, y = 0 } = {}) => ({
    x: scale * x + translateX,
    y: scale * y + translateY,
  })

  return derive
}

