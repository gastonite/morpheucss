




export const toRelative = ({
  originX = 0,
  originY = 0
}) => ({
  x = 0,
  y = 0
} = {}) => ({
  x: x - originX,
  y: y - originY,
})