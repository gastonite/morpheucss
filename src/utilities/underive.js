import { Transformation } from './Transformation'





export const underive = transformation => {

  const {
    translateX,
    translateY,
    scale,
  } = Transformation(transformation)

  const underive = ({ x = 0, y = 0 } = {}) => ({
    x: (x - translateX) / scale,
    y: (y - translateY) / scale,
  })

  return underive
}