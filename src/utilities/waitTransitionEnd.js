




export const waitTransitionEnd = element => new Promise(resolve => {

  const onTransitionEnd = event => {

    if (event.target !== element)
      return

    element.removeEventListener('transitionend', onTransitionEnd)

    resolve(event)
  }

  element.addEventListener('transitionend', onTransitionEnd)
})