




export const limitBelow = ({
  x: xMax = 0,
  y: yMax = 0
} = {}) => ({
  x = 0,
  y = 0
} = {}) => ({
  x: x > xMax
    ? xMax
    : x,
  y: y > yMax
    ? yMax
    : y,
})
