




export const Transformation = ({
  originX = 0,
  originY = 0,
  translateX = 0,
  translateY = 0,
  scale = 1,
} = {}) => ({
  originX,
  originY,
  translateX,
  translateY,
  scale,
})