import { Transformation } from './Transformation'





export const renderMatrix = transformation => {

  const {
    translateX,
    translateY,
    scale,
  } = Transformation(transformation)

  return `matrix(${scale}, 0, 0, ${scale}, ${translateX}, ${translateY})`
}