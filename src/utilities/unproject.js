import { Vector } from '../vector/vector.js'
import { toAbsolute } from './toAbsolute.js'
import { toRelative } from './toRelative.js'
import { Transformation } from './Transformation.js'
import { underive } from './underive.js'





export const unproject = transformation => {

  const {
    originX,
    originY,
    translateX,
    translateY,
    scale,
  } = Transformation(transformation)

  const unproject = point => {

    return Vector(point)
      .map(toRelative({ originX, originY }))
      .map(underive({
        scale,
        translateX,
        translateY,
      }))
      .map(toAbsolute({ originX, originY }))
  }


  return unproject
}