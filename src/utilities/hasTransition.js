




export const hasTransition = element => {

  const style = window.getComputedStyle(element)

  return style.getPropertyValue('transition-duration') !== '0s'
    && style.getPropertyValue('transition-property') !== 'none'
}