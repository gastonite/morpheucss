




export const constrainOutside = ({
  from: { x: xMax, y: yMax },
  to: { x: xMin, y: yMin },
}) => ({
  from: { x: fromX, y: fromY },
  to: { x: toX, y: toY }
}) => ({
  from: {
    x: fromX > xMax ? xMax : fromX,
    y: fromY > yMax ? yMax : fromY,
  },
  to: {
    x: toX < xMin ? xMin : toX,
    y: toY < yMin ? yMin : toY,
  }
})