import { identity } from '../utilities/identity.js'
import { origin, Vector } from '../vector/vector.js'





export const Box = ({
  from = origin,
  to = origin,
} = {}) => {

  const box = {
    from: Vector(from),
    to: Vector(to),
    map: (f = identity) => Box(f(box))
  }

  return box
}

