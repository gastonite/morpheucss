import { project } from '../utilities/project.js'
import { Box } from './box.js'





export const projectBox = transformation => box => {

  const { from, to } = Box(box)

  return {
    from: from.map(project(transformation)),
    to: to.map(project(transformation)),
  }
}