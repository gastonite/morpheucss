




export const constrainInside = ({
  from: { x: xMin, y: yMin },
  to: { x: xMax, y: yMax },
}) => ({
  from: { x: fromX, y: fromY },
  to: { x: toX, y: toY }
}) => ({
  from: {
    x: fromX < xMin ? xMin : fromX,
    y: fromY < yMin ? yMin : fromY,
  },
  to: {
    x: toX > xMax ? xMax : toX,
    y: toY > yMax ? yMax : toY,
  }
})
