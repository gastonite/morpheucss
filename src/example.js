import { Focusable } from './focusable.js'
import { Pannable } from './pannable.js'
import { transform } from './utilities/transform.js'
import { waitTransitionEnd } from './utilities/waitTransitionEnd.js'
import { Zoomable } from './zoomable.js'





const container = document.getElementById('container')

const { firstElementChild: area } = container
const { classList } = area

const step = .4
let _transformation

const transformArea = transform(area)

const enableTransition = async () => {
  classList.add('animated')
}

const disableTransition = async () => {
  await waitTransitionEnd(area)
  classList.remove('animated')
}

const contained = true
let _focused
const { zoom } = Zoomable({
  container,
  // minScale: 1,
  contained,
  transformElement: transformArea,
})

const { panBy } = Pannable({
  container,
  contained,
  transformElement: transformArea,
})
const { focus } = Focusable({
  container,
  contained,
  transformElement: transformArea,
  whenFocus: enableTransition,
  whenUnfocus: enableTransition,
  whenFocused: async () => {
    _focused = true
    await disableTransition()
  },
  whenUnfocused: async () => {
    await disableTransition()
    _focused = false
  }
})



const listenEvents = () => {

  container.addEventListener('wheel', async ({ currentTarget, pageX, pageY, deltaY, deltaX }) => {

    if (_focused)
      return

    let { left, top } = currentTarget.getBoundingClientRect()
    const offsetX = pageX - left
    const offsetY = pageY - top

    // Normalize to deltaX in case shift modifier is used on Mac
    const delta = deltaY === 0 && deltaX ? deltaX : deltaY
    _transformation = await zoom({
      x: offsetX,
      y: offsetY,
      ratio: Math.exp(((delta < 0 ? 1 : -1) * step) / 3),
      transformation: _transformation,
    })
  }, { passive: true })

  container.addEventListener('mousemove', async event => {

    if (_focused || event.buttons !== 1)
      return

    event.preventDefault()

    _transformation = await panBy({
      deltaX: event.movementX,
      deltaY: event.movementY,
      transformation: _transformation,
    })
  })

  container.addEventListener('dblclick', async ({ target }) => {

    _transformation = await focus({
      target,
      transformation: _transformation,
    })
  })

  document.querySelector('.zoomin').addEventListener('click', async () => {

    _transformation = await zoom({
      ratio: 1.2,
      transformation: _transformation,
    })
  })
}

const start = async () => {

  _transformation = await transformArea()

  listenEvents()
}

start()