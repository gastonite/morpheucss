import { Focusable } from './focusable.js'
import { Pannable } from './pannable.js'
import { Zoomable } from './zoomable.js'
import { noop } from './utilities/noop.js'
import { transform } from './utilities/transform.js'





export const Morpheucss = ({
  container,
  transformElement,
  zoomable,
  pannable,
  focusable,
  contained,
  minScale,
  maxScale,
  whenFocus,
  whenFocused,
  whenUnfocus,
  whenUnfocused,
} = {}) => {


  if (typeof transformElement !== 'function')
    transformElement = transform(container.firstElementChild)

  return {
    zoom: noop,
    panBy: noop,
    focus: noop,
    unfocus: noop,
    reset: transformElement.bind(undefined, undefined),
    ...focusable && Focusable({
      container,
      transformElement,
      whenFocus,
      whenFocused,
      whenUnfocus,
      whenUnfocused,
    }),
    ...zoomable && Zoomable({
      container,
      transformElement,
      contained,
      minScale,
      maxScale,
    }),
    ...pannable && Pannable({
      container,
      transformElement,
      contained,
    }),
  }
}